import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Account } from '../models/account.model'

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  readonly rootUrl = 'https://todo-ninja.azurewebsites.net';
  constructor(private http: HttpClient) { }
  registerUser(account: Account){
    return this.http.post(this.rootUrl + '/api/account/register', account);
  }


  userAuthentication(userName, password) {
    var data = "username=" + userName + "&password=" + password + "&grant_type=password";
    var reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded','No-Auth':'True' });
    return this.http.post('http://todo-ninja.azurewebsites.net/token', data, { headers: reqHeader });
  }
}
