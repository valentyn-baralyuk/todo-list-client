import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { INote } from '../Interfaces/INote';
import { ITask } from '../Interfaces/ITask';
import { Observable } from 'rxjs';
import { Note } from '../models/note.model';
import { map, mergeMap } from "rxjs/operators";
import { forkJoin, of } from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class NoteService {
  constructor(private http: HttpClient) { }
  private _Url: string = "https://todo-ninja.azurewebsites.net/api/notes";

  getNotesByUser(userId: string): Observable<INote[]> {
    return this.http.get<INote[]>(`https://todo-ninja.azurewebsites.net/api/users/${userId}/notes`);
  }



  postNote(note: Note) {
    return this.http.post<any>(this._Url, note);
  }

  deleteNote(Id: string) {
    return this.http.delete(this._Url + "/" + Id);
  }
  
  // getNotesWithTasks(): Observable<any[]> {
  //   return this.http.get('https://todo-ninja.azurewebsites.net/api/notes/')
  //     .pipe(map(res => res))
  //     .pipe(mergeMap((books: any[]) => {
  //       if (books.length > 0) {
  //         return forkJoin(
  //           books.map((note: any) => {
  //             return this.http.get('https://todo-ninja.azurewebsites.net/api/notes/' + note.Id + '/tasks/')
  //               .pipe(map((res: any) => {
  //                 let tasks: any = res;
  //                 note.Tasks = tasks;
  //                 return note;
  //               }));
  //           })
  //         );
  //       }
  //       return of([]);
  //     }));
  // }


}
