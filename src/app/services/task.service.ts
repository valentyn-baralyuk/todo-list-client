import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ITask } from '../Interfaces/ITask';
import { map } from 'rxjs/operators';
import { Task } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) { }

  private _url: string = "http://todo-ninja.azurewebsites.net/api/tasks";

  getTasks(): Observable<ITask[]> {
    return this.http.get<ITask[]>(this._url);
  }

  getTasksByNote(noteId: string) {
    return this.http.get<ITask[]>(`http://todo-ninja.azurewebsites.net/api/notes/${noteId}/tasks`);
  }

  getTask(Id: string) {
    return this.http.get(`${this._url}/${Id}`).subscribe(data => {
      console.log(data);
    });
  }

  postTask(task: Task) {
    return this.http.post<any>(this._url, task);
  }

  deleteTask(Id: string) {
    return this.http.delete(this._url + "/" + Id);
  }
}
