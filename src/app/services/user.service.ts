import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IUser } from '../Interfaces/IUser';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  private _url: string = "http://todo-ninja.azurewebsites.net/api/Users";

  getUsers(): Observable<IUser[]> {
    return this.http.get<IUser[]>(`${this._url}`);
  }
}
