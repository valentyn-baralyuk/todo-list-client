import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/account.service';
import { Account } from '../models/account.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  public account: Account = new Account("", "", "", "");
  constructor(private _accountService: AccountService, private router : Router) { }

  ngOnInit() {
  }

  OnSubmit(account: Account) {
    this._accountService.registerUser(account)
      .subscribe(data => {
        this.router.navigate(['/login']);
      });
  }
}
