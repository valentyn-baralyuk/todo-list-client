import { Component, OnInit, Input } from '@angular/core';
import { NoteService } from '../services/note.service';
import { Note } from '../models/note.model';
import { User } from '../models/user.model';
import * as _ from 'lodash';
import { UserService } from '../services/user.service';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent implements OnInit {
  public sub: string;
  public user: User;
  newDate: any = new Date().toJSON();
  public notes: any[] = [];
  public noteModel: Note;

  constructor(private _noteService: NoteService, private _userService: UserService) { }

  ngOnInit() {
    this.sub = localStorage.getItem('userId');
    this._userService.getUsers().subscribe(data => {
      this.user = _.find(data, o =>  o.AccountSubscription == this.sub);
      console.log(this.user);
      this._noteService.getNotesByUser(this.user.Id).subscribe(data => this.notes = data);
      this.noteModel = new Note("", "", this.newDate, this.user.Id, "")  
    })
  }

  onSubmitNote(noteForm: NgForm) {
    this._noteService.postNote(this.noteModel)
      .subscribe(data => this.notes.push(data));
    this._noteService.getNotesByUser(this.user.Id).subscribe(data => this.notes = data);
    noteForm.resetForm();
  }

  onDeleteNote(id: string) {
    this._noteService.deleteNote(id)
    .subscribe(data => _.remove(this.notes, n => n.Id === id));
  }
}
