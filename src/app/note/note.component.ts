import { Component, OnInit, Input } from '@angular/core';
import { TaskService } from '../services/task.service';
import { Task } from '../models/task.model';
import { INote } from '../interfaces/INote';
import * as _ from 'lodash';
import { ITask } from '../Interfaces/ITask';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {

  constructor(private _taskService: TaskService) { }
  @Input() public note: INote;
  taskModel = new Task("", "", "", "");
  tasks: ITask[];

  ngOnInit() {
    this._taskService.getTasksByNote(this.note.Id).subscribe(data => this.tasks = data);
  }

  onSubmitTask(Description: string) {
    this._taskService.postTask(new Task("", Description, this.note.Id, ""))
      .subscribe(data => this.tasks.push(data));
    this._taskService.getTasksByNote(this.note.Id).subscribe(data => this.tasks = data);
  }

  onDeleteTask(id: string) {
    this._taskService.deleteTask(id).subscribe(data => _.remove(this.tasks, t => t.Id === id));
  }
}

