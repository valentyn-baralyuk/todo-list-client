import { ITask } from './ITask';

export interface INote {
    Id: string;
    Title: string;
    CreatedAt: string;
    UserId: string;
    UserName: string;
    Tasks: ITask[];
}