export class Task{
  constructor(
   public Id:string,
   public Description:string,
   public NoteId:string,
   public NoteTitle:string, 
  ){}
}