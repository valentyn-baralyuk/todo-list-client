export class Note {
    constructor(
     public  Id:string,
     public Title:string,
     public CreatedAt:string,
     public UserId:string,
     public UserName:string
    ){}
}